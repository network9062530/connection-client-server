import socket

def main():
    host = 'localhost'
    port = 60444

    # Create a TCP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        # Connect to server
        s.connect((host, port))
        print(f"Connected to server: {host}:{port}")

        while True:
            # Receive the numsber from server
            data = s.recv(1024)
            nums = int(data.decode())
            print(f"Received from server: {nums}")

            if nums >= 100:
                break  # หยุดการทำงานของ Client เมื่อค่าตัวเลขเท่ากับ 100

            # Increment the numsber
            nums += 1

            # Send the updated numsber back to server
            s.send(str(nums).encode())

    except ConnectionRefusedError:
        print("Connection refused. Make sure the server is running.")
    finally:
        # Close the connection with the server
        s.close()
        print("Connection closed with server.")

if __name__ == "__main__":
    main()
